/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supermercado;

import java.util.ArrayList;

/**
 *
 * @author Eduardo
 */
public class Cliente {
    private String  nombre ,RFC;
    private ArrayList<historial> historialCompras = new ArrayList<historial>();

    public Cliente(String nombre, String RFC) {
        this.nombre = nombre;
        this.RFC = RFC;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public ArrayList<historial> getHistorialCompras() {
        return historialCompras;
    }

    public void setHistorialCompras(ArrayList<historial> historialCompras) {
        this.historialCompras = historialCompras;
    }
    
    
}
