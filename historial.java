/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supermercado;

/**
 *
 * @author Eduardo
 */
public class historial {
    private int cantidad_compra,no_compra;
    private String codigo_producto,fecha_compra;

    public historial(int cantidad_compra, int no_compra, String codigo_producto, String fecha_compra) {
        this.cantidad_compra = cantidad_compra;
        this.no_compra = no_compra;
        this.codigo_producto = codigo_producto;
        this.fecha_compra = fecha_compra;
    }

    public int getCantidad_compra() {
        return cantidad_compra;
    }

    public void setCantidad_compra(int cantidad_compra) {
        this.cantidad_compra = cantidad_compra;
    }

    public int getNo_compra() {
        return no_compra;
    }

    public void setNo_compra(int no_compra) {
        this.no_compra = no_compra;
    }

    public String getCodigo_producto() {
        return codigo_producto;
    }

    public void setCodigo_producto(String codigo_producto) {
        this.codigo_producto = codigo_producto;
    }

    public String getFecha_compra() {
        return fecha_compra;
    }

    public void setFecha_compra(String fecha_compra) {
        this.fecha_compra = fecha_compra;
    }
    
    
}
